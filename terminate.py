import psutil

proc_list=psutil.pids()

for member in proc_list:
    pid=psutil.Process(member)
    if pid.name()=="python3":
        pid.terminate()
        pid.wait()
#!/bin/bash

if ps -ef | grep /usr/bin/python3 | grep api.py; then
	printf "running"
    printf " "
else
	printf "Not Running"
    printf " "
	#export DISPLAY=:0.0
	cd /home/pi/videotron/
    git reset --hard
    git pull origin master
	chmod +x /home/pi/videotron/*
	chmod +x /home/pi/videotron/api.sh
	#chmod 777 /home/pi/videotron/*
	cd /home/pi/videotron/
	/usr/bin/python3 api.py 
fi	
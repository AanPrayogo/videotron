import os
#import time
from datetime import datetime
from database import MediaDatabase
from service import MediaService
from service import MediaOps
import logging
import logging.handlers
import sys
#from moviepy.editor import *
import sys
import subprocess
#import pygame
#import vlc

# path='E:\VIDEO'
path=sys.path[0]+'/video'
_LOG_=logging.getLogger()
clipFLag=True



def open_file(filename):
    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener="open" if sys.platform =="darwin" else "xdg-open"
        subprocess.call([opener,filename])

def check_title_on_advertisement():
    try:
        fileList=[]

        for file in os.listdir(path):
            if file.endswith('.mp4'):
                fileList.append(file)

        check_title=MediaService.get_title_on_table("Advertisement")
            
        if check_title is False:
            for File in fileList:
                param={
                    'Title' : File
                }
                MediaOps.insert_title_on_table("Advertisement",param)
        else:
            _LOG_.debug(("ALL TITLE ALREADY EXIST: "+str(check_title)))    
            print("ALL TITLE ALREADY EXIST")
        
        #_LOG_.debug(("GET ALL TITLE FILE: "+str(fileList)))    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] play_tvc: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def play_ads(ads):
    try:
        ads_=path+"/"+ads+str(".mp4")
        value = os.system("omxplayer -o both --no-osd -b "+str(ads_))
        return value
        #os.system ("xrefresh -display :0")
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] play_tvc: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

# def play_clip(File):
#     global clipFLag
#     try:
#         clip = VideoFileClip(path+"/"+File+".mp4")
#         #clip=clip.resize(newsize=(1280,720))
#         clip.set_fps(100)
#         res=clip.preview(fullscreen=True)
#     except Exception as e:
#         print("Failed to load MP4 file : " + str(e))
#         _LOG_.debug(("looping play ERROR : "+str(e)))
#         return False

# def play_vids(vids):
#     global clipFLag
#     try:
#         media_player = vlc.MediaPlayer(path+"/"+vids+".mp4")      
#         media_player.toggle_fullscreen()        
#         # media = vlc.Media(path+"/"+vids+".mp4")        
#         # media_player.set_media(media)   
#         em = media_player.event_manager()
#         em.event_attach(vlc.EventType.MediaPlayerEndReached, onEnd)     
#         media_player.play()
#     except Exception as e:
#         print("Failed to load MP4 file : " + str(e))
#         _LOG_.debug(("looping play ERROR : "+str(e)))
#         return False

# def onEnd(event):
#     global doTrashCode
#     if event.type == vlc.EventType.MediaPlayerEndReached:
#         doTrashCode = True

def check_AdsId_on_DailyPlay():
    try:
        list_dailyplay=MediaService.get_AdsId_playlist_by_dateplay()
        if not list_dailyplay:
            active_vids=MediaService.get_Id_active()
            for File in active_vids:
                param={"AdsId":File,
                        "DatePlay":MediaService.get_date_today()}
                MediaOps.insert_table_DailyPlay(param)
            _LOG_.debug(("ALL ACTIVE TITLE ALREADY SET: "+str(active_vids)))    
        else:
            _LOG_.info(("ALL ACTIVE TITLE ALREADY EXIST"))
            param={
                "StatusPlay":"active",
                "Status":"enabled",
                "DeleteStatus":0
            }
            active_id=MediaService.get_Id_active()
            for _id in active_id:
                if _id not in list_dailyplay:
                    param={"AdsId":_id,
                        "DatePlay":MediaService.get_date_today()
                    }
                    MediaOps.insert_table_DailyPlay(param)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] check_title_on_DailyPlay: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_mp4_on_folder():
    try:
        fileList=[]
        for file in os.listdir(path):
            if file.endswith('.mp4'):
                fileList.append(file.split(".mp4")[0])
        return fileList
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_mp4_on_folder: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        return False
    
def download_vid_by_LinkSource():
    try:
        exist_file=get_mp4_on_folder()
        _LOG_.debug(("[ALL] Title On FOLDER EXISTING : "+str(exist_file)))    
        
        if len(exist_file) == 0:    #MP4 FILE IN FOLDER DOESN'T EXIST
            _LOG_.debug(("[LENGTH] exist file: "+str(len(exist_file))))    
            all_link=MediaService.get_LinkSource_from_db()
            if all_link is not False:
                for link in all_link:
                    MediaService.get_video_from_link(link)
            else:
                _LOG_.warning(("[FAILED] get all link: "+str(all_link)))    
        else:   #MP4 EXIST, DOING CHECKING FILE EXISTING
            _LOG_.debug(("[LENGTH] exist file: "+str(len(exist_file))))    
            all_id=MediaService.get_Id_active()
            
            _LOG_.info(("[ALL] Id On FOLDER db : "+str(all_id)))    
            _LOG_.info(("[ALL] Title On FOLDER EXISTING : "+str(exist_file)))    

            for vids in all_id:
                if vids not in exist_file:
                    link_=MediaService.get_link_by_Id(vids)
                    print(link_)
                    if link_ is not False:
                        download_status=MediaService.get_video_from_link(link_)
                        if not download_status:
                            param={}
                            print("[FAILED] download vids from link : "+str(link_))
                            _LOG_.warning(("[FAILED] download vids from link : "+str(link_)))
                        
                            param["AdsId"]=vids
                            MediaOps.delete_DailyPlay_by_AdsId(param)
                    else:
                        _LOG_.warning(("[FAILED] get_link_by_title: "+str(link_)))    
                else:
                    print("[CHECKING] vids already exist: "+str(vids))
                    _LOG_.info(("[CHECKING] vids already exist: "+str(vids)))    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] download_vid_by_LinkSource: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
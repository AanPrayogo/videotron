from database import MediaDatabase

def insert_title_on_table(table,param):
    sql='INSERT INTO '+ table +' (Title) VALUES (:Title)'
    return MediaDatabase.insert_or_update_database(sql,param)

def insert_table_DailyPlay(param):
    sql='INSERT INTO DailyPlay (AdsId,DatePlay) VALUES (:AdsId,:DatePlay)'
    return MediaDatabase.insert_or_update_database(sql,param)

def check_title_on_table(table):
    sql='SELECT Title FROM ('+ table +')'
    return MediaDatabase.get_result_void(sql)

def update_count_media(param):
    sql='UPDATE DailyPlay SET PlayCount=PlayCount+1 WHERE AdsId=:AdsId AND DatePlay=:DatePlay'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_current_viewer(param):
    sql='SELECT Viewer FROM DailyPlay WHERE AdsId=:AdsId AND DatePlay=:DatePlay'
    return MediaDatabase.get_result_set(sql,param)

def update_counter_viewer(param):
    sql='UPDATE DailyPlay SET Viewer=:Viewer WHERE AdsId=:AdsId AND DatePlay=:DatePlay'
    return MediaDatabase.insert_or_update_database(sql,param)

def update_LastPlay(param):
    sql='UPDATE DailyPlay SET LastPlay=:LastPlay WHERE AdsId=:AdsId AND DatePlay=:DatePlay'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_data_table():
    try:
        sql='SELECT * FROM Advertisement'
        return MediaDatabase.get_result_void(sql)
    except Exception as e:
        print("[FAILED GET data_table] get_data_table : "+str(e))
        return False

def get_Date_by_id(param):
    sql='SELECT StartDate, EndDate FROM Advertisement WHERE Id=:Id'
    return MediaDatabase.get_result_set(sql,param)

def update_StatusPlay_by_id(param):
    sql='UPDATE Advertisement SET StatusPlay=:StatusPlay WHERE Id=:Id'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_active_Id(param):
    sql='SELECT Id FROM Advertisement WHERE StatusPlay=:StatusPlay AND Status=:Status AND DeleteStatus=:DeleteStatus'
    return MediaDatabase.get_result_set(sql,param)

def get_dateplay_by_title(param):
    sql='SELECT DatePlay FROM DailyPlay WHERE Title=:Title'
    return MediaDatabase.get_result_set(sql,param)

def set_dateplay_by_title(param):
    sql='INSERT INTO DailyPlay (DatePlay) VALUES (:DatePlay) WHERE Title=:Title'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_AdsId_playlist_by_date(param):
    sql='SELECT AdsId FROM DailyPlay WHERE DatePlay=:DatePlay'
    return MediaDatabase.get_result_set(sql,param)

def get_all_value_db(param):
   sql='SELECT Advertisement.*, DailyPlay.AdsId, DailyPlay.PlayCount,DailyPlay.Viewer,DailyPlay.DatePlay, DailyPlay.LastPlay , DailyPlay.TotalCorrupt FROM Advertisement LEFT JOIN DailyPlay ON Advertisement.Id=DailyPlay.AdsId WHERE DatePlay=:DatePlay'
   return MediaDatabase.get_result_set(sql,param)

def set_ads_source(param):
    sql='INSERT INTO Advertisement (Id, Title, StartDate, EndDate, LinkSource, Status, StatusPlay, LastUpdate, DeleteStatus, SyncFlag, Duration) VALUES (:Id, :Title, :StartDate, :EndDate, :LinkSource, :Status, :StatusPlay, :LastUpdate, :DeleteStatus, :SyncFlag, :Duration)'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_LinkSource():
    sql='SELECT LinkSource FROM Advertisement'
    return MediaDatabase.get_result_void(sql)

def get_id_from_advertisement():
    sql='SELECT Id FROM Advertisement'
    return MediaDatabase.get_result_void(sql)

def update_ads_source(param):
    sql='UPDATE Advertisement SET Title=:Title, StartDate=:StartDate, EndDate=:EndDate, LinkSource=:LinkSource, Status=:Status, StatusPlay=:StatusPlay, LastUpdate=:LastUpdate, DeleteStatus=:DeleteStatus, SyncFlag=:SyncFlag, Duration=:Duration WHERE Id=:Id'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_Id_from_advertisement():
    sql='SELECT Id FROM Advertisement'
    return MediaDatabase.get_result_void(sql)

def get_LinkSource_by_Id(param):
    sql='SELECT LinkSource FROM Advertisement WHERE Id=:Id'
    return MediaDatabase.get_result_set(sql,param)

def get_Id_by_link(param):
    sql='SELECT Id FROM Advertisement WHERE LinkSource=:LinkSource'
    return MediaDatabase.get_result_set(sql,param)

def delete_DailyPlay_by_AdsId(param):
    sql='DELETE FROM DailyPlay WHERE AdsId=:AdsId'
    return MediaDatabase.delete_record(sql,param)

def update_TotalCorrupt(param):
    sql='UPDATE DailyPlay SET TotalCorrupt=TotalCorrupt+1 WHERE AdsId=:AdsId AND DatePlay=:DatePlay'
    return MediaDatabase.insert_or_update_database(sql,param)

def insert_HistoryCorrupt(param):
    sql='INSERT INTO HistoryCorrupt (AdsId,CorruptDate) VALUES (:AdsId,:CorruptDate)'
    return MediaDatabase.insert_or_update_database(sql,param)

def get_duration_by_id(param):
    sql='SELECT Duration FROM Advertisement WHERE Id=:Id'
    return MediaDatabase.get_result_set(sql,param)
from service import MediaOps
import logging
import requests
import os
import sys    
import pytube
import datetime
import time
import gdown

_LOG_=logging.getLogger()
path=sys.path[0]+'/video'
#url="http://172.20.200.10:5003" ##OFFICE IP
#url="http://192.168.240.245:5003"
countView=0

def get_host_by_txt():
    try:
        host=open(sys.path[0]+"/host.txt","r").read()
        print(host)
        _LOG_.debug(("Succes get_host_by_txt = "+str(host)))
        return host
    except Exception as e:
        _LOG_.warning(("Failed to get_host_by_txt "+str(e)))
        print("Failed to get_host_by_txt = " + str(e))
        return False

def get_title_on_table(table):
    try:
        result_title=[]
        title_list=MediaOps.check_title_on_table(table)
        #print("title list : ",title_list)
        if len(title_list) !=0:
            for title in title_list:
                result_title.append(title['Title'])
            return result_title
        else:
            return False
    except Exception as e:
        print("failed  get_title_on_table : "+str(e))
        return False 

def get_counter_viewer():
    global countView
    try:
        url=get_host_by_txt()
        resp=requests.get("http://"+url+":5003"+"/counter/getCount")
        print(resp.json())
        response_=resp.json()
        _LOG_.debug(("[DEBUG] response from getCount API "+str(response_)))
        countView=response_["counter"]
        print("countView = "+str(countView))
        _LOG_.debug(("[DEBUG] countView = "+str(countView)))
        return int(countView)
    except Exception as e:
        _LOG_.warning(("Failed to get counter viewer : " + str(e)))
        return 0

def reset_counter_viewer():
    try:
        url=get_host_by_txt()
        resp=requests.get("http://"+url+":5003"+"/counter/resetCount")
        print(resp.json())
        status=resp.json()["isSucces"]
        if status == "true":
            return True
        else:
            return False
    except Exception as e:
        _LOG_.warning(("Failed to reset_counter_viewer : " + str(e)))
        return False

def get_video_from_link(link):
    try:
        print("LINK : ", link)
        path=sys.path[0]+'/video' 
        param_link={
            "LinkSource":link
        }
        Id=MediaOps.get_Id_by_link(param_link)[0]["Id"]
        print("Id = ", Id)
        output=path+"/"+str(Id)+".mp4"
        gdown.download(link,output)
        _LOG_.debug(("Done download " +str(link)+" Id=" +str(Id)))

        return True
        # try: 
        #     yt = pytube.YouTube(link)
        # except Exception as e: 
        #     print("Connection Error="+str(e))
        #     _LOG_.warning(("Connection Error="+str(e)))

        # try:
        #     stream=yt.streams.filter(res="720p").first()
        #     stream.download(path,str(Id)+".mp4")
        #     print("Done download " +str(link)+" Id=" +str(Id))
        #     _LOG_.debug(("Done download " +str(link)+" Id=" +str(Id)))
        #     return True
        # except Exception as e:
        #     print("Failed download " +str(e))
        #     _LOG_.debug(("Failed download file from link : " +str(link) +"with error : " +str(e)))
        #     return False
            # try:
            #     stream=yt.streams.filter(res="720p").first()
            #     stream.download(path,str(Id)+".mp4")
            #     print("Done download " +str(link)+" Id=" +str(Id))
            #     _LOG_.debug(("Done download " +str(link)+" Id=" +str(Id)))
            # except Exception as e:
            #     print("Failed download " +str(e))
            #     _LOG_.debug(("Failed download file from link : " +str(link) +"with error : " +str(e)))

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_video_from_link: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        return False


def get_video_by_txt():
    try:
        link=open(sys.path[0]+"/link.txt","r").read().split("\n")
        _LOG_.debug(("Succes get_video_by_txt = "+str(link)))
        return link
    except Exception as e:
        _LOG_.warning(("Failed to get_video_by_txt "+str(e)))
        print("Failed to get_video_by_txt = " + str(e))
        return False

def download_video_by_link():
    try:
        Link=get_video_by_txt()
        if Link is not False:
            for link in Link:
                get_video_from_link(link)
    except Exception as e:
        _LOG_.warning(("Failed to download_video_by_link"+str(e)))
        print("Failed to download_video_by_link = " + str(e))
        return False
    
def get_today_stamp():
    try:
        date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        #print(date)
        _LOG_.info(("get_today_stamp "+str(date)))
        return date
    except Exception as e:
        _LOG_.warning(("[ERROR] get_today_stamp "+str(date)))
        return False 
    
def get_today_hour():
    try:
        date=datetime.datetime.now().strftime("%H:%M:%S")
        #print(date)
        _LOG_.info(("get_today_hour "+str(date)))
        return date
    except Exception as e:
        _LOG_.warning(("[ERROR] get_today_hour "+str(date)))
        return False
        
def get_date_today():
    try:
        date=datetime.datetime.now().strftime("%Y-%m-%d")
        #print(date)
        _LOG_.info(("get_date_today "+str(date)))
        return date
    except Exception as e:
        _LOG_.warning(("[ERROR] get_date_today "+str(date)))
        return False

def get_date_yesterday():
    try:
        today=datetime.datetime.now()
        yesterday=(today-datetime.timedelta(days=1))
        yesterday=yesterday.strftime("%Y-%m-%d")
        _LOG_.info(("get_date_yesterday = "+str(yesterday)))
        return yesterday
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_date_yesterday: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        return False
    
def check_overdate_file():
    idList=[]

    try:
        today=get_date_today()
        all_id=MediaOps.get_Id_from_advertisement()
        for Id in all_id:
            idList.append(Id["Id"])
        print(idList)
        _LOG_.info(("[CHECKING] ALL ID IN CHECK OVERDATE = "+str(idList)))

        for id_ in idList:
            param={}
            param["Id"]=id_
            date=MediaOps.get_Date_by_id(param)[0]
            end_date=date["EndDate"]
            start_date=date["StartDate"]
            #print(end_date)
            _LOG_.info(("[CHECKING] StartDate= "+str(start_date) +"," + "EndDate = "+ str(end_date) +"," + "id = "+ str(id_)))

            if start_date<=today<=end_date:
                param['StatusPlay']='active'
            else:
                param['StatusPlay']='inactive'
            MediaOps.update_StatusPlay_by_id(param)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] check_overdate_file: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_Id_active():
    activeId=[]

    try:
        param={
            "StatusPlay":"active",
            "Status":"enabled",
            "DeleteStatus":0
        }
        all_active=MediaOps.get_active_Id(param)

        if len(all_active) !=0:
            for Id in all_active:
                activeId.append(Id["Id"])
            #print(activeTitle)
            _LOG_.info(("[SUCCES] Get all actice title"+str(activeId)))
            return activeId
        else:
            _LOG_.warning(("[FAILED] Get all actice title"+str(all_active)))
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_title_active: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_curr_date_by_title(title): 
    try:
        res_date=MediaOps.get_dateplay_by_title(title)
        date=res_date[0]
        print(date)
        print(len(date))
        if len(date)!=0:
            print(date)
            _LOG_.info(("[SUCCES] get_curr_date_by_title"+str(date)))
            return date[0]["DatePlay"]
        else:
            _LOG_.debug(("[DEBUG] get_curr_date_by_title"+str(date)))
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_curr_date_by_title: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_AdsId_playlist_by_dateplay(): 
    try:
        list_=[]
        param={
            "DatePlay":get_date_today()
        }
        playlist_date=MediaOps.get_AdsId_playlist_by_date(param)
        _LOG_.info(("playlist_date " + str(playlist_date) + str(len(playlist_date))))

        if len(playlist_date)==0:
            return False
        else:
            for play in playlist_date:
                list_.append(play["AdsId"])
            return list_
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_title_palylist_by_dateplay: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_all_data_from_db():
    try:
        param={
            'DatePlay' : get_date_yesterday()
        }
        data_db=MediaOps.get_all_value_db(param)
        _LOG_.debug(("[DEBUG] ALL DB VALUE "+str(data_db)))
        
        if len (data_db)!=0:
            return data_db
        else:
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_all_data_from_db: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def set_advertisement_from_gui(source):
    try:
        exist_id=[]
        _LOG_.info(("[SOURCE] source param = " + str(source) ))
        
        res_id=MediaOps.get_id_from_advertisement()
        _LOG_.info(("[RAW] exist id in ads = " + str(res_id) ))

        if len(res_id) ==0:  #INSERT WHEN ADS IS EMPTY
            for source_param in source:
                param={}
                res_set=False
                
                param["Id"]=source_param["Id"]
                param["Title"]=source_param["Title"]
                param["StartDate"]=source_param["StartDate"]
                param["EndDate"]=source_param["EndDate"]
                param["LinkSource"]=source_param["LinkSource"]
                param["Status"]=source_param["Status"]
                param["StatusPlay"]=source_param["StatusPlay"]
                param["LastUpdate"]=get_today_stamp()
                param["DeleteStatus"]=source_param['DeleteStatus']
                param["SyncFlag"]=1
                param["Duration"]=source_param['Duration']
            
                res_set=MediaOps.set_ads_source(param)
                
            if res_set:
                res={}
                res['isSucces']="true"
                res['action']="insert to db ads"
                res['condition']="title on db ads empty"
                _LOG_.info(("[SUCCES] SET DB advertisement = " + str(res_set) + str(res)))
                return res
            else:
                res={}
                res['isSucces']="false"
                res['action']="insert to db ads"
                res['condition']="title on db ads empty"
                _LOG_.warning(("[FAILED] SET DB advertisement = " + str(res_set) + str(res)))
                return res
        else: #CHECK IF ID EXIST
            for id_ in res_id:
                exist_id.append(id_["Id"])
            _LOG_.debug(("[EXISTING]exist id in ads = " + str(exist_id) ))
            
            for source_param in source:
                res_set=False
                res_update=False
                param={}
                
                param["Id"]=source_param["Id"]
                param["Title"]=source_param["Title"]
                param["StartDate"]=source_param["StartDate"]
                param["EndDate"]=source_param["EndDate"]
                param["LinkSource"]=source_param["LinkSource"]
                param["Status"]=source_param["Status"]
                param["StatusPlay"]=source_param["StatusPlay"]
                param["LastUpdate"]=get_today_stamp()
                param["DeleteStatus"]=source_param['DeleteStatus']
                param["SyncFlag"]=source_param['SyncFlag']
                param["Duration"]=source_param['Duration']

                if source_param["Id"] in exist_id:
                    res_update=MediaOps.update_ads_source(param)
                else:
                    res_set=MediaOps.set_ads_source(param)
                
            if res_update:
                res={}
                res['isSucces']="true"
                res['action']="update to db ads"
                res['condition']="title on db ads already exist"
                _LOG_.info(("[SUCCES] SET DB advertisement = " + str(res_update) + str(res)))
                return res
            if res_set:
                res={}
                res['isSucces']="true"
                res['action']="insert to db ads"
                res['condition']="title on db ads already exist"
                _LOG_.info(("[SUCCES] SET DB advertisement = " + str(res_set) + str(res)))
                return res
            else:
                res={}
                res['isSucces']="false"
                res['action']="insert to db ads"
                res['condition']="title on db ads already exist"
                _LOG_.warning(("[FAILED] SET DB advertisement = " + str(res_set) + str(res)))
                return res
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] set_advertisement_from_gui: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_LinkSource_from_db():
    try:
        res_link=[]
        link_db=MediaOps.get_LinkSource()
        if len(link_db)!=0:
            for link in link_db:
                res_link.append(link["LinkSource"])
            return res_link
        else:
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_LinkSource_from_db: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_Id_from_table_ads():
    try:
        res_id=[]
        id_db=MediaOps.get_Id_from_advertisement()  
        _LOG_.warning(("[RESULT] id_db advertisement = " + str(id_db) ))

        if len(id_db)!=0:
            for id_ in id_db:
                res_id.append(str(id_["Id"]))
            return res_id
        else:
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_Title_from_db: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_link_by_Id(Id):
    try:
        print(Id)
        param={
            'Id':Id
        }
        res_link=MediaOps.get_LinkSource_by_Id(param)
        _LOG_.debug(("[INFO] result link advertisement = " + str(res_link) ))

        if len(res_link)!=0:
            result=res_link[0]["LinkSource"]
            return result
        else:
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_link_by_Id: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))

def get_duration_by_Id(ads_id):
    try:
        param={
            "Id":ads_id
        }
        print("param get duration :",param)
        file_duration=MediaOps.get_duration_by_id(param)
        _LOG_.debug(("[INFO] file_duration advertisement = " + str(file_duration)))

        if len(file_duration)==0:
            return False
        else:
            return file_duration[0]["Duration"]
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_duration_by_AdsId: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
    

                
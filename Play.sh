#!/bin/bash

if ps -ef | grep /usr/bin/python3 | grep VideoTron.py; then
	printf "running"
    printf " "
else
	printf "Not Running"
    printf " "
	export DISPLAY=:0.0
	cd /home/pi/videotron/
	/usr/bin/python3 VideoTron.py
fi	
import flask
from flask import request,jsonify
import sqlite3
import os
import sys
import socket
from service import MediaService
from database import MediaDatabase
import logging
import logging.handlers
import json

app=flask.Flask(__name__)
app.config["DEBUG"]=True

db_name="MediaPlayer.db"
ip=None
_LOG_=None
tempCounter=[]
totalTemp=0

books = [
    {'id': 0,
     'title': 'A Fire Upon the Deep',
     'author': 'Vernor Vinge',
     'first_sentence': 'The coldsleep itself was dreamless.',
     'year_published': '1992'},
    {'id': 1,
     'title': 'The Ones Who Walk Away From Omelas',
     'author': 'Ursula K. Le Guin',
     'first_sentence': 'With a clamor of bells that set the swallows soaring, the Festival of Summer came to the city Omelas, bright-towered by the sea.',
     'published': '1973'},
    {'id': 2,
     'title': 'Dhalgren',
     'author': 'Samuel R. Delany',
     'first_sentence': 'to wound the autumnal city.',
     'published': '1975'}
]


@app.route('/',methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"

@app.errorhandler(404)
def page_not_found():
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

@app.route('/books/all',methods=['GET'])
def books_all():
    return jsonify (books) 

@app.route('/counter/storeTempCount',methods=['POST'])
def set_temp_counter():
    param=request.json
    counter_val=param['counter']
    tempCounter.append(counter_val)
    print(tempCounter)
    
    res={"service":"storeTempCount",
        "isSucces":"true",
        "tempCount":tempCounter}
    _LOG_.info(("[RESULT] getCount = "+str(json.dumps(res))))
    return jsonify(res)  

@app.route('/counter/getCount',methods=['GET'])
def get_counter_viewer():
    totalTemp=sum(tempCounter)
    res={"service":"getCount",
        "counter":totalTemp}
    _LOG_.info(("[RESULT] getCount = "+str(json.dumps(res))))
    return jsonify(res)

@app.route('/counter/resetCount',methods=['GET'])
def reset_counter_viewer():
    global totalTemp,tempCounter
    totalTemp=0
    tempCounter=[]
    res={"service":"resetCount",
        "isSucces":"true"}
    
    print("Succes to Reset temp Counter in API")
    _LOG_.info(("Succes to reset parameter =" + str(totalTemp) +str(tempCounter)))
    return jsonify(res)

@app.route('/ads/getAdsData',methods=['GET'])
def get_ads_data_from_db():
    all_data=MediaService.get_all_data_from_db()
    _LOG_.debug(("SEND DB DATA TO GUI =" + str(all_data)))
    #print(all_data)
    res={
        "service":"getAdsData",
        "data":all_data,
    }
    return jsonify(res)

@app.route('/ads/storeAdsSource',methods=['POST'])
def set_ads_source():
    status=None
    source=None
    result={}
    
    param=request.json
    #print(param)
    _LOG_.debug(("[INFO] RAW DATA  = " + str(param)))
    status=param["isSucces"]
    
    if status=="true":
        source=param["source"]
        resFlag=MediaService.set_advertisement_from_gui(source)
        if resFlag['isSucces']=="true":
            result["isSucces"]="true"
            result["message"]="succes update advertisement on raspi"
            result["action"]=resFlag["action"]
            result["condition"]=resFlag["condition"]
            result["data"]=MediaService.get_Id_from_table_ads()
            return jsonify(result),200
    else:
        result["isSucces"]="false"
        result["message"]="succes status from client is false"
        print("[FAILED] set_ads_source = " + str(status))
        _LOG_.debug(("[FAILED] set_ads_source = " + str(status)))    
        return jsonify(result),400

@app.route("/ads/status", methods=["GET"])
def ads_status():
    res={
        "status":"ON",
        "service" : "get raspi status"
    }
    return jsonify(res)    
        
def get_conn():
    try:
        conn=sqlite3.connect(sys.path[0] + '/database/'+ db_name)
        conn.row_factory = dict_factory
        return conn
    except Exception as e:
        print("failed to get connection "+str(e))

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return dict(((k, v) for k, v in d.items() if v is not None))


def get_host_by_txt():
    try:
        host=open(sys.path[0]+"/host.txt","r").read()
        print(host)
        _LOG_.debug(("Succes get_host_by_txt = "+str(host)))
        return str(host)
    except Exception as e:
        _LOG_.warning(("Failed to get_host_by_txt "+str(e)))
        print("Failed to get_host_by_txt = " + str(e))
        return False

def configuration_log():
    global _LOG_
    try:
        if not os.path.exists(sys.path[0] + '/log/'):
            os.makedirs(sys.path[0] + '/log/')
        handler = logging.handlers.TimedRotatingFileHandler(filename=sys.path[0] + '/log/base.log',
                                                            when='MIDNIGHT',
                                                            interval=1,
                                                            backupCount=60)
        logging.basicConfig(handlers=[handler],
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                            datefmt='%d/%m %H:%M:%S')
        _LOG_ = logging.getLogger()
    except Exception as e:
        print("Logging Configuration ERROR : ", e)

if __name__=="__main__":
    configuration_log()
    ip=get_host_by_txt()
    #ip="192.168.240.245"
    MediaDatabase.check_db_exist()
    app.run(host=ip,port=5003)


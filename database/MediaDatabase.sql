DROP TABLE IF EXISTS DailyPlay;
CREATE TABLE DailyPlay(
    Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    AdsId VARCHAR(255) ,
    PlayCount INT  DEFAULT(0),
    Viewer INT  DEFAULT(0),
    DatePlay DATETIME,
    LastPlay DATETIME,
    TotalCorrupt INT DEFAULT(0)
);

DROP TABLE IF EXISTS Advertisement;
CREATE TABLE Advertisement(
    Id VARCHAR(255) NOT NULL PRIMARY KEY,
    Title VARCHAR(255),
    StartDate DATETIME,
    EndDate DATETIME,
    LinkSource VARCHAR(255),
    StatusPlay VARCHAR(255),
    Status VARCHAR(255),
    LastUpdate DATETIME,
    DeleteStatus INT DEFAULT (0),
    SyncFlag INT DEFAULT(0),
    Duration iNT DEFAULT(0)
);

DROP TABLE IF EXISTS HistoryCorrupt;
CREATE TABLE HistoryCorrupt(
    Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    AdsId VARCHAR(255),
    CorruptDate DATETIME
);
import sqlite3
import os
import sys
import threading
import logging

_LOG_=logging.getLogger()
lock=threading.Lock()

db_name='MediaPlayer.db'

def create_db():
    try:
        conn=get_conn()
        with open (sys.path[0]+'/database/MediaDatabase.sql') as f:
            conn.cursor().executescript(f.read())
    except Exception as e:
        print("failed to build db : "+str(e))

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return dict(((k, v) for k, v in d.items() if v is not None))

def get_result_void(sql):
    try:
        lock.acquire()
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql)
        result = cursor.fetchall()
        _LOG_.info((sql, str(result)))
    finally:
        lock.release()
    return result

def check_db_exist():
    try:
        if not os.path.isfile(sys.path[0] + '/database/' + db_name):
            print("db not exist, create DB")
            create_db()
        else:
            print("database already exist")
    except Exception as e:
        print("failed check_db_exist : "+str(e))

def get_result_set(sql, parameter):
    try:
        lock.acquire()
        _LOG_.info(("sql : "+ str(sql) + "Parameter" + str(parameter)))
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql, parameter)
        result = cursor.fetchall()
    except Exception as e:
        lock.release()
        lock.acquire()
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql, parameter)
        result = cursor.fetchall()
    finally:
        lock.release()

    _LOG_.info(("Result Parameter ", str(result)))
    return result

def get_conn():
    conn = sqlite3.connect(sys.path[0] + '/database/' + db_name)
    conn.row_factory = dict_factory
    return conn

def insert_or_update_database(sql, parameter):
    try:
        lock.acquire()
        _LOG_.info((sql, str(parameter)))
        conn__ = get_conn()
        conn__.execute(sql, parameter)
        conn__.commit()
        return True
    except Exception as e:
        lock.release()
        lock.acquire()
        _LOG_.info((sql, str(parameter)))
        conn__ = get_conn()
        conn__.execute(sql, parameter)
        conn__.commit()
        return True
    finally:
        lock.release()

def delete_record(sql,param):
    try:
        lock.acquire()
        _LOG_.info(sql)
        conn__ = sqlite3.connect(sys.path[0] + '/database/' + db_name)
        conn__.cursor().execute(sql,param)
        conn__.commit()
    finally:
        lock.release()

# if __name__=="__main__":
#     create_db()
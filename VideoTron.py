import os
import time
from datetime import datetime
from database import MediaDatabase
from service import MediaService
from service import MediaOps
import logging
import logging.handlers
import sys
#from moviepy.editor import *
import sys
import subprocess
#import pygame
import configparser
import player
#import vlc
path=sys.path[0]+'/video'
clipFLag=True
FILE_NAME=sys.path[0]+"/config.conf"

def configuration_log():
    global _LOG_
    try:
        if not os.path.exists(sys.path[0] + '/log/'):
            os.makedirs(sys.path[0] + '/log/')
        handler = logging.handlers.TimedRotatingFileHandler(filename=sys.path[0] + '/log/base.log',
                                                            when='MIDNIGHT',
                                                            interval=1,
                                                            backupCount=60)
        logging.basicConfig(handlers=[handler],
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                            datefmt='%d/%m %H:%M:%S')
        _LOG_ = logging.getLogger()
    except Exception as e:
        print("Logging Configuration ERROR : ", e)

def init_conf():
    global conf
    conf=configparser.ConfigParser()
    conf.read(FILE_NAME)

def get_value(section, option):
    if conf is None:
        init_conf()
        #check_file_config()
    try:
        return conf.get(section, option)
    except configparser.NoOptionError:
        return None
    except configparser.NoSectionError:
        return None
    
def set_value(section, option, value):
    if conf is None:
        init_conf()
    if section not in conf.sections():
        add_section(section)
    conf.set(section, option, value)
    save_file()
    return value

def add_section(section):
    conf.add_section(section)

def save_file():
    conf.write(open(FILE_NAME, 'w'))

def play_ads():
    global clipFLag

    try:
        active_vids=MediaService.get_Id_active()
        print("active_vids "+str(active_vids))
        _LOG_.debug(("[GET ALL] active_vids DailyPlay "+ str(active_vids)))

        daily_vids=MediaService.get_AdsId_playlist_by_dateplay()
        print("daily vids "+str(daily_vids))
        
        exist_vids=player.get_mp4_on_folder()
        
        for file_vids in exist_vids:
            if file_vids not in active_vids:
                os.remove(path+"/"+file_vids+".mp4")
                print("delete vids : "+str(file_vids))
                _LOG_.debug(("delete vids : "+str(file_vids)))
                
        
        for vid in daily_vids:
            if vid not in active_vids:
                daily_vids.remove(vid)
                # os.remove(path+"/"+vid+".mp4")
                # print("remove vids : "+str(vid))
                # _LOG_.debug(("remove vids : "+str(vid)))

        _LOG_.debug(("[GET ALL] Play on DailyPlay "+ str(daily_vids)))

        if active_vids is not False:
            while True:
                for File in daily_vids:
                    viewer=0
                    current_viewer=0
                    total_viewer=0
                    reset_status=False
                    param={}
                    
                    start_time=time.time()
                    value=player.play_ads(File)         #play vids on screen
                    end_time=time.time()
                    
                    elapsed_time=end_time-start_time
                    vids_duration=MediaService.get_duration_by_Id(File)
                    
                    if value == 0 : #non corrupt file      
                        if int(elapsed_time)<=int(0.6*vids_duration):     #if video break while running   
                            print("FAILED to play file (BREAK): " + str(File))
                            _LOG_.warning(("FAILED to play file (BREAK): " + str(File)))
            
                            param['AdsId']=File
                            param['DatePlay']=MediaService.get_date_today()
                            MediaOps.update_TotalCorrupt(param)
                            
                            param['LastPlay']=MediaService.get_today_hour()
                            MediaOps.update_LastPlay(param)
                            param.pop('DatePlay')
                            param.pop('LastPlay')
                            
                            param["CorruptDate"]= MediaService.get_today_stamp()
                            MediaOps.insert_HistoryCorrupt(param)
                            
                            
                        else:   #Succes to play vids 
                            param['AdsId']=File
                            param['DatePlay']=MediaService.get_date_today()
                            MediaOps.update_count_media(param)
                            
                            param['LastPlay']=MediaService.get_today_hour()
                            MediaOps.update_LastPlay(param)
                            param.pop('LastPlay')
                            
                            title_viewer=MediaOps.get_current_viewer(param)
                            current_viewer=title_viewer[0]['Viewer']
                            viewer=MediaService.get_counter_viewer()
                            
                            total_viewer=current_viewer+viewer
                            print("total_viewer = ", total_viewer)
                            param['Viewer']=total_viewer   
                            MediaOps.update_counter_viewer(param)

                            reset_status=MediaService.reset_counter_viewer()
                            if reset_status:
                                _LOG_.info(("succes to reset counter"))
                            else:
                                _LOG_.warning(("FAILED to reset counter"))
                    else:       #corrupt file
                        print("FAILED to play file (CORRUPT): " + str(File))
                        _LOG_.warning(("FAILED to play file (CORRUPT): " + str(File)))
                        
                        param['AdsId']=File
                        param['DatePlay']=MediaService.get_date_today()
                        MediaOps.update_TotalCorrupt(param)
                        
                        param['LastPlay']=MediaService.get_today_hour()
                        MediaOps.update_LastPlay(param)
                        param.pop('DatePlay')
                        param.pop('LastPlay')
                        
                        param["CorruptDate"]= MediaService.get_today_stamp()
                        MediaOps.insert_HistoryCorrupt(param)
                    time.sleep(1)  #add little wait before proceed to next video
        else:
            _LOG_.warning(("FAILED to get play vids "+str(active_vids)))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] play_ads: ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        
        
if __name__=="__main__":
    if not os.path.exists(sys.path[0] + '/video'):
        print("make video dir")
        os.makedirs(sys.path[0] + '/video')
    configuration_log()
    MediaDatabase.check_db_exist()
    MediaService.check_overdate_file()
    player.check_AdsId_on_DailyPlay()
    player.download_vid_by_LinkSource()
    play_ads()